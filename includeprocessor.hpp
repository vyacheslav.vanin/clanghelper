#pragma once
#include <functional>

#include "clanghelper/arghelper.hpp"
#include "vvvclanghelper.hpp"

struct InclusionDirectiveInfo {
    InclusionDirectiveInfo(clang::SourceLocation hashLoc,
                           const clang::Token& includeTok,
                           clang::StringRef fileName, bool isAngled,
                           clang::CharSourceRange filenameRange,
                           const clang::FileEntry* file,
                           clang::StringRef searchPath,
                           clang::StringRef relativePath,
                           const clang::Module* imported,
                           clang::SrcMgr::CharacteristicKind FileType,
                           const clang::CompilerInstance& compiler)
        : filenameRange(filenameRange), hashLoc(hashLoc), FileType(FileType),
          fileName(fileName), relativePath(relativePath),
          searchPath(searchPath), file(file), imported(imported),
          includeTok(includeTok), isAngled(isAngled), compiler(compiler)
    {
    }

    clang::CharSourceRange filenameRange;
    clang::SourceLocation hashLoc;
    clang::SrcMgr::CharacteristicKind FileType;
    clang::StringRef fileName;
    clang::StringRef relativePath;
    clang::StringRef searchPath;
    const clang::FileEntry* file;
    const clang::Module* imported;
    const clang::Token& includeTok;
    bool isAngled;
    const clang::CompilerInstance& compiler;
};
using include_processor_t = std::function<void(const InclusionDirectiveInfo&)>;

void visit_include_decls(int argc, const char** argv,
                         const include_processor_t& f,
                         const std::vector<std::string>& my_params);
void visit_include_decls(const include_processor_t& f,
                         const std::string& filename,
                         const std::vector<std::string>& compiler_flags,
                         const std::vector<std::string>& tool_flags);
