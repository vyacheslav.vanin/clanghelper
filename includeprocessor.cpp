#include "includeprocessor.hpp"

#include "clanghelper/arghelper.hpp"
#include <clang/Frontend/FrontendActions.h>

namespace {
using string_list = std::vector<std::string>;
using ParamList = string_list;

class CallbacksProxy : public clang::PPCallbacks {
public:
    CallbacksProxy(const clang::CompilerInstance& compiler_instance,
                   const include_processor_t& processor);

public:
    virtual void InclusionDirective(
        clang::SourceLocation HashLoc, const clang::Token& IncludeTok,
        clang::StringRef FileName, bool IsAngled,
        clang::CharSourceRange FilenameRange, const clang::FileEntry* File,
        clang::StringRef SearchPath, clang::StringRef RelativePath,
        const clang::Module* Imported,
        clang::SrcMgr::CharacteristicKind FileType) override;

private:
    const clang::CompilerInstance& compiler;
    const include_processor_t& processor;
};

CallbacksProxy::CallbacksProxy(const clang::CompilerInstance& ci,
                               const include_processor_t& processor)
    : compiler(ci), processor(processor)

{
}

void CallbacksProxy::InclusionDirective(
    clang::SourceLocation hashLoc, const clang::Token& includeTok,
    clang::StringRef fileName, bool isAngled,
    clang::CharSourceRange filenameRange, const clang::FileEntry* file,
    clang::StringRef searchPath, clang::StringRef relativePath,
    const clang::Module* imported, clang::SrcMgr::CharacteristicKind FileType)
{
    processor({hashLoc, includeTok, fileName, isAngled, filenameRange, file,
               searchPath, relativePath, imported, FileType, compiler});
}

class Tool : public clang::PreprocessOnlyAction {
public:
    Tool(const ParamList& params, const include_processor_t& processor)
        : params(params), processor(processor)
    {
    }

    virtual void ExecuteAction() override
    {
        const auto& ci = getCompilerInstance();
        auto& pp = ci.getPreprocessor();
        auto cbp = std::make_unique<CallbacksProxy>(ci, processor);
        pp.addPPCallbacks(std::move(cbp));

        clang::PreprocessOnlyAction::ExecuteAction();
    }

    bool usesPreprocessorOnly() const override { return true; }

    const ParamList& params;
    const include_processor_t& processor;
};
} // namespace

void visit_include_decls(int argc, const char** argv,
                         const include_processor_t& f,
                         const std::vector<std::string>& my_params)
{
    if (argc < 2) {
        std::cerr << "fatal error: no input files" << std::endl;
    }

    const auto params = CxxToolArgs(argc, argv, my_params);

    for (const auto& name : params.getFilenames()) {
        const auto& flags = params.getFlagsForSource(name);
        const auto& tool_flags = params.getCustomFlags();
        visit_include_decls(f, name, flags, tool_flags);
    }
}

void visit_include_decls(const include_processor_t& f,
                         const std::string& filename,
                         const string_list& compiler_flags,
                         const string_list& tool_flags)
{
    using namespace clang::tooling;
    const auto code = getSourceFromFile(filename.c_str());
    runToolOnCodeWithArgs(new Tool(tool_flags, f), code.c_str(), compiler_flags,
                          filename.c_str());
}
